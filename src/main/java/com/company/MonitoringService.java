package com.company;

import org.apache.log4j.Logger;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MonitoringService {
    public static final Logger log = Logger.getLogger(MonitoringService.class);

    public static final int TIMEOUT = 1000;
    public static final int PC_NUMBER = 5;
    public static final int REAL_PC_ID = 1;

    public static void main(String[] args) {
        log.info("Monitoring service was launched");
        MonitoringService ms = new MonitoringService();
        List<PC> pcList;
        try {
            pcList = ms.initPCList();
        } catch (UnknownHostException e) {
            log.error("Failed to run monitoring service", e);
            return;
        }

        while (true) {
            Map<String, Integer> dynamicStat;
            try {
                dynamicStat = Utils.getDynamicPCStat();
            } catch (Exception e) {
                log.error("Failed to obtain pcu and memory usage", e);
                return;
            }

            Integer cpuLoad;
            Integer memoryLoad;
            if (dynamicStat != null && dynamicStat.containsKey("cpuLoad") && dynamicStat.containsKey("memoryLoad")) {
                cpuLoad = dynamicStat.get("cpuLoad");
                memoryLoad = dynamicStat.get("memoryLoad");
            } else {
                log.error("Incorrect pcu or memory usage value");
                return;
            }


            for (PC pc : pcList) {
                // Рандомизируем загрузку ЦП и ОЗУ для фековых PC
                if (pc.getId() != REAL_PC_ID) {
                    cpuLoad += Utils.getRandomDeltaPercent(cpuLoad);
                    memoryLoad += Utils.getRandomDeltaPercent(memoryLoad);
                }

                pc.setCpuLoad(cpuLoad);
                pc.setMemoryLoad(memoryLoad);

                try {
                    Utils.sendStatistics(pc.toString());
                } catch (IOException e) {
                    log.error("Failed to send statistics", e);
                }
            }

            try {
                Thread.sleep(TIMEOUT);
            } catch (InterruptedException e) {
                log.warn("Interrupted!", e);
            }
        }

    }

    private List<PC> initPCList() throws UnknownHostException {
        List<PC> pcList = new ArrayList<>();
        Map<String, String> hostInfo = null;
        try {
            hostInfo = Utils.getHostInfo();
        } catch (UnknownHostException e) {
            log.error("Couldn't init PC list");
            throw e;
        }
        String name = hostInfo.get("name");
        String ipAddress = hostInfo.get("ipAddress");

        ipAddress = ipAddress.substring(0, ipAddress.length() - 1);
        for (int i = 0; i < PC_NUMBER; i++) {
            PC pc = new PC();
            pc.setId(i + 1);
            pc.setName(name);
            pc.setIpAddress(ipAddress + i);

            pcList.add(pc);
        }

        return pcList;
    }

}
