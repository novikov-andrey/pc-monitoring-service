package com.company;

import kamon.sigar.SigarProvisioner;
import org.apache.log4j.Logger;
import org.hyperic.sigar.*;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Utils {
    public static final Logger log = Logger.getLogger(Utils.class);

    private Utils() {}

    public static Map<String, String> getHostInfo() throws UnknownHostException {
        Map<String, String> hostInfo = new HashMap<>();
        try {
            hostInfo.put("name", InetAddress.getLocalHost().getHostName());
            hostInfo.put("ipAddress", InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            log.error("Couldn't obtain host info");
            throw e;
        }
        return hostInfo;
    }

    public static Map<String, Integer> getDynamicPCStat() throws Exception {
        Map<String, Integer> dynamicStat = new HashMap<>();

        try {
            SigarProvisioner.provision();
        } catch (Exception e) {
            log.error("Couldn't obtain dynamic info due to problems with dll");
            throw e;
        }

        Sigar sigar = new Sigar();
        Mem mem;
        CpuPerc cpuperc;
        try {
            mem = sigar.getMem();
            cpuperc = sigar.getCpuPerc();
        } catch (SigarException se) {
            log.error("Couldn't obtain dynamic info due to problems with Sigar API");
            throw se;
        }

        dynamicStat.put("cpuLoad", (int) (cpuperc.getSys() * 100));
        dynamicStat.put("memoryLoad", (int) mem.getUsedPercent());

        return dynamicStat;
    }

    public static void sendStatistics(String statInfo) throws IOException {
        Socket socket = null;
        DataOutputStream os;
        try {
            socket = new Socket("localhost", 5400);
            os = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            os.writeBytes(statInfo);
            os.flush();
        } catch (IOException e) {
            log.error("Failed to send statistics");
            throw e;
        } finally {
            try {
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                log.warn("Failed to close the socket", e);
            }
        }
    }

    public static int getRandomDeltaPercent(int loadPercent) {
        return new Random().nextInt(100) - loadPercent;
    }
}
