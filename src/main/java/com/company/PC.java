package com.company;

import java.time.LocalDateTime;

public class PC {
    private Integer id;
    private String name;
    private String ipAddress;
    private Integer cpuLoad;
    private Integer memoryLoad;

    @Override
    public String toString() {
        return name + id + " " + ipAddress + " " + cpuLoad + " " + memoryLoad + " " + LocalDateTime.now().toLocalDate() + " " + LocalDateTime.now().toLocalTime();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getCpuLoad() {
        return cpuLoad;
    }

    public void setCpuLoad(Integer cpuLoad) {
        this.cpuLoad = cpuLoad;
    }

    public Integer getMemoryLoad() {
        return memoryLoad;
    }

    public void setMemoryLoad(Integer memoryLoad) {
        this.memoryLoad = memoryLoad;
    }
}
